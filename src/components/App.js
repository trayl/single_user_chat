import React, { Component } from 'react';
import '../styles/App.css';
import 'react-chat-elements/dist/main.css';
import ChatWindow from './ChatWindow.js';

class App extends Component {
    render() {
        return (
            <ChatWindow />
        );
    }
}

export default App;