import React, { Component } from 'react';


class Message extends Component {

    renderEditMode() {
        return (
            <input id={'edit-text-field'} className={'speech-bubble'} type={'text'} default={this.props.msg_body} />
        );
    }

    renderNormal() {
        return (
            <p className={'speech-bubble wrap_text'}>{this.props.msg_body}</p>
        );
    }

    render() {
        if(this.props.edit_mode) {
            return this.renderEditMode();
        }
        return this.renderNormal();
    }
}

export default Message;