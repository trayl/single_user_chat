import React, { Component } from 'react';
import '../styles//Message.css';
import Message from './Message.js';
import edit_logo from './../resources/edit_pen.png';
import delete_logo from './../resources/delete_cross.png';


class ChatWindow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message_render_list: [],    // Get from Relay allMessages
            message_meta_list: []
        };
    }

    updateState(key) {
        let edit_message_meta = [];
        edit_message_meta = edit_message_meta.concat(this.state.message_meta_list[key]);

        let edit_message_render = edit_message_meta.map(msg => (
              <div key={msg.message_id}>
                <img className={'avatar-image'} src={msg.picture_url} alt={'avatar-logo'} />
                <Message
                    msg_body={msg.message_body}
                    edit_mode={msg.edit_mode}
                />
                <img className={'edit-logo'} src={edit_logo} alt='edit' onClick={this.editClicked.bind(this, msg.message_id)} />
                <img className={'delete-logo'} src={delete_logo} alt='delete' onClick={this.deleteClicked.bind(this, msg.message_id)} />

              </div>
            ));

        this.state.message_render_list[key] = edit_message_render;
    }

    updateKeys(key) {
        
        this.state.message_meta_list[key].message_id = key;
        this.updateState(key);
        
    }

    // Handle click of edit button
    editClicked(key) {

        this.state.message_meta_list[key].edit_mode = !this.state.message_meta_list[key].edit_mode;
        this.updateState(key);

        // If done editing
        if(!this.state.message_meta_list[key].edit_mode) {
            
            this.state.message_meta_list[key].message_body = document.getElementById("edit-text-field").value;
            this.updateState(key);
        }

        this.forceUpdate();

        return;
    }

    // Handle click of delete button
    deleteClicked(key) {

        // Remove
        this.state.message_meta_list.splice(key, 1);
        this.state.message_render_list.splice(key, 1);

        // Update key of proceeding messages
        for(let i = key; i < this.state.message_meta_list.length; ++i) {
            this.updateKeys(i);
        }

        this.forceUpdate();

        return;
    }


    handleSubmit(event) {        
        let full_render_list = this.state.message_render_list;
        let full_meta_list = this.state.message_meta_list;

        let new_message_json = [
            {
                message_id: this.state.message_render_list.length,
                picture_url: 'https://scontent-ort2-2.xx.fbcdn.net/v/t31.0-8/26172658_140996509943588_4203264619578463287_o.jpg?oh=cbce8f512ea580176445c23651ac406e&oe=5AFF4274',
                message_body: document.getElementById("text-input").value,
                edit_mode: false
            }
        ];

        const new_render_message = new_message_json.map(msg => (
              <div key={msg.message_id}>
                <img className={'avatar-image'} src={msg.picture_url} alt={'avatar-logo'} />
                <Message
                    msg_body={msg.message_body}
                    edit_mode={msg.edit_mode}
                />
                <img className={'edit-logo'} src={edit_logo} alt='edit' onClick={this.editClicked.bind(this, msg.message_id)} />
                <img className={'delete-logo'} src={delete_logo} alt='delete' onClick={this.deleteClicked.bind(this, msg.message_id)} />

              </div>
            ));

        full_render_list = full_render_list.concat(new_render_message);
        full_meta_list = full_meta_list.concat(new_message_json[0]);

        this.setState({
            message_render_list: full_render_list,
            message_meta_list: full_meta_list
        });

        document.getElementById("text-input").value = '';
        event.preventDefault();
    }

    componentWillMount() {
        // Fetch All Messages    
    }

    render() {
        return (
            <div>
                <div className={'message-counter'}>
                    <p> {this.state.message_render_list.length} messsages </p>
                </div>
                
                <div className={'message-window'}>
                    { this.state.message_render_list }
                </div>

                <form onSubmit={this.handleSubmit.bind(this)} id={'input-form'}>
                    <label>
                        <input type="text" name="text-input" id={'text-input'}  />
                    </label>
                    <input type="submit" value="Submit" className={'no-display'} />
                </form>
            </div>
        );
    }
}

export default ChatWindow;



