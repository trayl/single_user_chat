import React, { Component } from 'react'
import Message from './Message'
import { createFragmentContainer, graphql } from 'react-relay';

class MessageList extends Component {

    render() {

        const messagesToRender = [{
            message_id: '1',
            message_body: 'This is a sample message with id 1',
            picture_url: 'https://scontent-ort2-2.xx.fbcdn.net/v/t31.0-8/26172658_140996509943588_4203264619578463287_o.jpg?oh=cbce8f512ea580176445c23651ac406e&oe=5AFF4274'
        }, {
            message_id: '2',
            message_body: 'This is a sample message with id 1',
            picture_url: 'https://scontent-ort2-2.xx.fbcdn.net/v/t31.0-8/26172658_140996509943588_4203264619578463287_o.jpg?oh=cbce8f512ea580176445c23651ac406e&oe=5AFF4274'
        }];

        return (
            <div>
                {messagesToRender.map(msg => (
                    <Message key={msg.id} msg={msg}/>
                ))}
            </div>
        )
    }

}
export default MessageList;
// export default createFragmentContainer(MessageList, graphql`
//   fragment MessageList_viewer on Viewer {
//     allLinks(last: 100, orderBy: createdAt_DESC) @connection(key: "MessageList_allMessages", filters: []) {
//       edges {
//         node {
//           ...Message_link
//         }
//       }
//     }
//   }`
// )