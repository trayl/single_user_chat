const {
  Environment,
  Network,
  RecordSource,
  Store,
} = require('relay-runtime');

const source = new RecordSource();
const store = new Store(source);
const network = Network.create((operation, variables) => {

	return fetch('http://localhost:4000/graphql', {
		method: 'POST',
		mode: 'no-cors',
		headers: {
			'Content-Type': 'application/graphql',
			'Accept': 'application/graphql'
		},
		body: JSON.stringify({
		    query: operation.text,
		    variables
		}),
	}).then(response => {
		return response.json();
	});
});

// const handlerProvider = null;

const environment = new Environment({
  // handlerProvider, // Can omit.
  network,
  store,
});

export default environment;