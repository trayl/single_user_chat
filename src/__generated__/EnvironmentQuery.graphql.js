/**
 * @flow
 * @relayHash c9f3686a83e00e8c15d0413394de5b2c
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type EnvironmentQueryResponse = {|
  +allMessages: ?$ReadOnlyArray<?{|
    +message_id: ?string;
  |}>;
|};
*/


/*
query EnvironmentQuery {
  allMessages {
    message_id
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "EnvironmentQuery",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "Message",
        "name": "allMessages",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "message_id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "RootQueryType"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "EnvironmentQuery",
  "query": {
    "argumentDefinitions": [],
    "kind": "Root",
    "name": "EnvironmentQuery",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "Message",
        "name": "allMessages",
        "plural": true,
        "selections": [
          {
            "kind": "ScalarField",
            "alias": null,
            "args": null,
            "name": "message_id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query EnvironmentQuery {\n  allMessages {\n    message_id\n  }\n}\n"
};

module.exports = batch;
