# Single User Chat

### Interview Project

---
## Installation and Run Process
1. Clone repository
  `git clone git@gitlab.com:trayl/single_user_chat.git`
2. Run the following commands
```
cd single_user_chat
npm install
npm start
```

---
### Resources
- Relay Tutorial: https://www.youtube.com/watch?v=zrUfWvPs1RA
- Chat Elements: https://www.npmjs.com/package/react-chat-elements#button-component
- Message Bubble: https://leaverou.github.io/bubbly/
- Avatar CSS: https://stackoverflow.com/questions/26681059/create-a-circle-avatar-from-a-rectangle-image-keeping-proportions-and-just-using
- Input Box: https://codepen.io/juliendargelos/pen/MJjJZm
---