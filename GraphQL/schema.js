const {
	GraphQLObjectType,
	GraphQLString,
	GraphQLInt,
	GraphQLSchema,
	GraphQLList,
	GraphQLNonNull
} = require('graphql');

// Hardcoded Data
const messages = [
	{message_id: "0", name: "Tristan Ray", body: "This is a test message", picture_url: "https://media.licdn.com/mpr/mpr/shrinknp_200_200/AAEAAQAAAAAAAAWiAAAAJDg5OWRjOGY1LTkyMTgtNDQxYS05MjMzLTA4ZGNjYzExYjQwNQ.jpg", timestamp: "1/11/2017 10:25"}
	];

// Message Type
const MessageType = new GraphQLObjectType({
	name: 'Message',
	fields: () => ({
		message_id: {type: GraphQLString},
		name: {type: GraphQLString},
		body: {type: GraphQLString},
		picture_url: {type: GraphQLString},
		timestamp: {type: GraphQLString},
	})
});

// Root Query
const RootQuery = new GraphQLObjectType({
	name: 'RootQueryType',
	fields: {
		oneMessage: {
			type: MessageType,
			args: {
				message_id: {type: GraphQLString}
			},
			resolve(parentValue, args){
				for(let i=0; i < messages.length; ++i) {
					if(messages[i].message_id == args.message_id) {
						return messages[i];
					}
				}
			}
		},
		allMessages: {
			type: new GraphQLList(MessageType),
			resolve(parentValue, args) {
				return messages;
			}
		}
	}
});


const mutation = new GraphQLObjectType ({
	name: 'Mutation',
	fields: {
		addMessage:{
			type: MessageType,
			args: {
				name: {type: new GraphQLNonNull(GraphQLString)},
				body: {type: new GraphQLNonNull(GraphQLString)},
				picture_url: {type: new GraphQLNonNull(GraphQLString)},
				timestamp: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve(parentValue, args) {
				args['message_id'] = (messages.length + 1);
				messages.push(args);
				return messages[messages.length - 1];
			}
		},
		deleteMessage:{
			type: new GraphQLList(MessageType),
			args: {
				message_id: {type: new GraphQLNonNull(GraphQLString)}
			},
			resolve(parentValue, args) {

				for(let i=0; i < messages.length; ++i) {
					if(messages[i].message_id == args.message_id) {
						messages.splice(i, 1);
						return messages[i];
					}
				}
				
			}
		},
		editMessage:{
			type: new GraphQLList(MessageType),
			args: {
				message_id: {type: new GraphQLNonNull(GraphQLString)},
				body: {type: new GraphQLNonNull(GraphQLString)},
			},
			resolve(parentValue, args) {
				for(let i=0; i < messages.length; ++i) {
					if(messages[i].message_id == args.message_id) {
						messages[i].body = args.body;
						return messages;
					}
				}
			}
		}
	},
});

module.exports = new GraphQLSchema({
	query: RootQuery,
	mutation: mutation
});
